const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/messages', (req, res) => {
        res.send(db.getMessages());
    });

    router.post('/messages', (req, res) => {
        const message = req.body;

        db.addMessage(message).then(result => {
            res.send(result);
        })

    });

    return router;
};

module.exports = createRouter;